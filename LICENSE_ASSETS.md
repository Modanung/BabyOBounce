## Used licenses

Public domain, _[CC0](https://creativecommons.org/publicdomain/zero/1.0/), [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/)/[4.0](https://creativecommons.org/licenses/by/4.0/), [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)_

## Assets by type, author and license

### Various
##### Author irrelevant
- CC0
    + Screenshots/*
    + Resources/Materials/*
    + Resources/Shaders/*
    + Resources/Techniques/*
    + Resources/RenderPaths/*
    + Resources/Particles/*
    + Resources/PostProcess/*
    + Resources/Textures/LUT.*
    + Resources/Textures/Ramp.*
    + Resources/Textures/Spot.*
    + Resources/Textures/Smoke.*

### Music
##### Loyalty Freak Music
- CC0
    + Resources/Music/Loyalty Freak Music - MEGA METAL.ogg [| Source |](https://freemusicarchive.org/music/Loyalty_Freak_Music/HYPER_METAL_/Loyalty_Freak_Music_-_HYPER_METAL__-_03_MEGA_METAL/)
    
### Samples

##### Abacagi
- CC-BY 4.0
    + Resources/Samples/Bush.ogg [| Source |](https://freesound.org/people/Abacagi/sounds/511346/)
    
##### Modanung
- CC0
    + Resources/Samples/Bounce.ogg
        * Made with SuperCollider
        
##### Crinkem
- CC0
    + Resources/Samples/Tube.ogg [| Source |](https://freesound.org/people/Crinkem/sounds/492027/)
    
##### Breviceps
- CC0
    + Resources/Samples/Splat.ogg [| Source |](https://freesound.org/people/Breviceps/sounds/445109/)

##### yottasounds
- CC0
    + Resources/Samples/Squish.ogg [| Source |](https://freesound.org/people/yottasounds/sounds/232135/)
    
##### lesbaton100
- CC-BY 3.0
    + Resources/Samples/Woosh.ogg [| Source |](https://freesound.org/people/lebaston100/sounds/264438/)
        
### 2D   
##### Rick Montgomery
- Public domain
    + Resources/Fonts/Yikes.ttf [| Source |](https://www.dafont.com/yikes.font)
    
##### Modanung
- CC-BY-SA 4.0
    + Raw/*
    + Resources/Textures/Tree.png
    + Resources/Textures/Fire.png  
    + Resources/Textures/BloodSplat.png
    + Resources/Textures/Baby.png

### 3D
##### Modanung
- CC-BY-SA 4.0
    + Blends/*
    + Resources/Models/*