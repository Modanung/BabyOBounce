/* Baby-O-Bounce
// Copyright (C) 2017 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef BABYCAM_H
#define BABYCAM_H

#include "mastercontrol.h"

namespace Dry {
class Drawable;
class Node;
class Scene;
class Sprite;
class Viewport;
class RenderPath;
class Camera;
}

using namespace Dry;

class BabyCam : public LogicComponent
{
    DRY_OBJECT(BabyCam, LogicComponent);
    friend class MasterControl;
    friend class InputMaster;
    friend class Player;
public:
    BabyCam(Context *context);
    static void RegisterObject(Context *context);
    virtual void OnNodeSet(Node *node);

    Camera* camera_;

    Vector3 GetWorldPosition();
    Quaternion GetRotation();
private:
    void Update(float timeStep);

    Vector3 smoothTargetPosition_;
    Vector3 smoothTargetVelocity_;

    void SetupViewport();
    void Lock(SharedPtr<Node> node);
};

#endif
