
#include "trampoline.h"



void Trampoline::RegisterObject(Context* context)
{
    context->RegisterFactory<Trampoline>();
}

Trampoline::Trampoline(Context* context): Controllable(context),
    trampolineModel_{ nullptr },
    pId_{},
    sinceHit_{ 100.0f },
    maxMoveSpeed_{ 10.0f },
    moveSpeed_{}
{
}

void Trampoline::OnNodeSet(Node* node)
{ if(!node) return;

    node_->AddTag("Trampoline");
    Node* modelNode{ node_->CreateChild("Model") };
//    modelNode->SetScale(Vector3(1.2f, 0.1f, 1.2f));
    trampolineModel_ = modelNode->CreateComponent<AnimatedModel>();
    trampolineModel_->SetModel(RES(Model, "Models/Trampoline.mdl"));
    trampolineModel_->SetMaterial(1, RES(Material, "Materials/Trampoline.xml"));
    trampolineModel_->SetCastShadows(true);

    RigidBody* rb{ node_->CreateComponent<RigidBody>() };
    rb->SetRestitution(0.89f);

    node_->CreateComponent<CollisionShape>()
         ->SetTriangleMesh(RES(Model, "Models/Trampoline_COLLISION.mdl"));//Cylinder(1.5f, 0.1f, Vector3::DOWN * 0.1f);

    SubscribeToEvent(node_, E_NODECOLLISIONSTART, DRY_HANDLER(Trampoline, HandleNodeCollisionStart));
}

void Trampoline::Set(int pId)
{
    pId_ = pId;

    SceneObject::Set(Vector3(0.0f, 0.5f, LANE_WIDTH * pId_));

    trampolineModel_->SetMaterial(0, MC->GetPlayerMaterial(pId));
}

void Trampoline::Update(float timeStep)
{
    Move(timeStep);
    Stretch(timeStep);
}

void Trampoline::Move(float timeStep)
{
    Vector3 worldPos{ node_->GetWorldPosition() };
    float worldPosX{ node_->GetWorldPosition().x_ };
    float absPosX{ Abs(worldPosX) };

    moveSpeed_ *= absPosX > 4.0f ? 0.8f * (1.0f - (TRAMPOLINE_MAXPOSX - absPosX) * 0.125f)
                                 : 0.8f;

    moveSpeed_ += move_.x_ * (2.0f) + Sign(moveSpeed_) * move_.x_ * moveSpeed_ * 0.25f;

    if (moveSpeed_ > maxMoveSpeed_
     || moveSpeed_ < -maxMoveSpeed_)
    {
        moveSpeed_ = Sign(moveSpeed_) * maxMoveSpeed_;
    }

    node_->Translate(Vector3::RIGHT * timeStep * moveSpeed_, TS_WORLD);

    worldPos = node_->GetWorldPosition();
    worldPosX = node_->GetWorldPosition().x_;

    if (worldPosX < -TRAMPOLINE_MAXPOSX
     || worldPosX > TRAMPOLINE_MAXPOSX)
    {
        worldPos.x_ = Sign(worldPosX) * TRAMPOLINE_MAXPOSX;
        node_->SetWorldPosition(worldPos);
        moveSpeed_ = 0.0f;
    }

    node_->SetRotation({ 0.f, 0.f, aim_.x_ * -11.f });
}

void Trampoline::Stretch(float timeStep)
{
    sinceHit_ += timeStep;
    float amplitude{ Pow(Max(0.0f, 0.4f * (2.0f - sinceHit_)), 4.0f) };
    float stretch{ -amplitude * LucKey::Sine(sinceHit_ * 25.0f) };
    trampolineModel_->SetMorphWeight(0, stretch);
}

void Trampoline::HandleNodeCollisionStart(StringHash eventType, VariantMap& eventData)
{ (void)eventType;

    Node* otherNode{ static_cast<Node*>(eventData[NodeCollisionStart::P_OTHERNODE].GetPtr()) };

    if (otherNode->HasTag("Bounce"))
    {
        sinceHit_ = 0.0f;

        const float freq{ 20000.f + 10000.f * node_->GetWorldPosition().DistanceSquaredToPoint(otherNode->GetWorldPosition()) };
        PlaySample("Bounce.ogg", freq);
    }
}
