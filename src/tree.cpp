#include "baby.h"

#include "tree.h"

void Tree::RegisterObject(Context* context)
{
    context->RegisterFactory<Tree>();
}

Tree::Tree(Context* context): SceneObject(context)
{
}

void Tree::OnNodeSet(Node* node)
{ if(!node) return;

    node_->SetRotation(Quaternion(Random(360.0f), Vector3::UP));

    StaticModel* treeModel{ node_->CreateComponent<StaticModel>() };
    treeModel->SetModel(RES(Model, "Models/Tree.mdl"));
    treeModel->SetMaterial(RES(Material, "Materials/Tree.xml"));
    treeModel->SetCastShadows(true);

    RigidBody* rigidBody{ node_->CreateComponent<RigidBody>() };
    rigidBody->SetTrigger(true);
    CollisionShape* trigger{ node_->CreateComponent<CollisionShape>() };
    trigger->SetCapsule(2.0f, 5.0f, Vector3::UP * 3.0f);

    SubscribeToEvent(node_, E_NODECOLLISIONSTART, DRY_HANDLER(Tree, HandleNodeCollision));
}

void Tree::Update(float timeStep)
{
}

void Tree::HandleNodeCollision(StringHash eventType, VariantMap& eventData)
{ (void)eventType;

    Node* otherNode{ static_cast<Node*>(eventData[NodeCollisionStart::P_OTHERNODE].GetPtr()) };
    RigidBody* otherBody{ static_cast<RigidBody*>(eventData[NodeCollisionStart::P_OTHERBODY].GetPtr()) };

    if (otherNode->HasComponent<Baby>())
    {
        otherBody->ApplyForce(-otherBody->GetLinearVelocity() * 100.0f);

        if (otherNode->GetWorldPosition().y_ > 1.5f)
            PlaySample("Bush.ogg", 0.f, .5f);
    }
}




