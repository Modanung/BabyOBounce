/* Baby-O-Bounce
// Copyright (C) 2017 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "babycam.h"
#include "inputmaster.h"
#include "player.h"

void BabyCam::RegisterObject(Context *context)
{
    context->RegisterFactory<BabyCam>();
}

BabyCam::BabyCam(Context* context):
    LogicComponent(context),
    smoothTargetPosition_{Vector3::ZERO},
    smoothTargetVelocity_{Vector3::ZERO}
{
}

void BabyCam::OnNodeSet(Node *node)
{ if (!node) return;

    float viewRange{ 50.0f };

    camera_ = node_->CreateComponent<Camera>();
    camera_->SetFarClip(viewRange);
    camera_->SetNearClip(0.1f);
    camera_->SetFov(23.0f);

    Zone* zone{ node_->CreateComponent<Zone>() };
    zone->SetBoundingBox(BoundingBox(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)));
    zone->SetAmbientColor(Color(0.15f, 0.3f, 0.8f));
    zone->SetFogColor(Color(0.1f, 0.1f, 0.2f, 1.0f));
    zone->SetFogStart(30.0f);
    zone->SetFogEnd(viewRange);

    SoundListener* listener{ node_->CreateComponent<SoundListener>() };
    GetSubsystem<Audio>()->SetListener(listener);

    SetupViewport();

    node_->SetPosition(Vector3(7.0f, 7.0f, -20.0f));
    node_->LookAt(Vector3(1.0f, 2.5f, 0.0f));
}

void BabyCam::SetupViewport()
{
    //Set up a viewport to the Renderer subsystem so that the 3D scene can be seen
    SharedPtr<Viewport> viewport(new Viewport(context_, GetScene(), camera_));

    //Add anti-asliasing and bloom
    SharedPtr<RenderPath> effectRenderPath_{ viewport->GetRenderPath() };
    effectRenderPath_->Append(RES(XMLFile, "PostProcess/FXAA3.xml"));
    effectRenderPath_->SetEnabled("FXAA3", true);
    effectRenderPath_->Append(RES(XMLFile, "PostProcess/BloomHDR.xml"));
    effectRenderPath_->SetShaderParameter("BloomHDRThreshold", 0.5f);
    effectRenderPath_->SetShaderParameter("BloomHDRMix", Vector2(.9f, 0.3f));
    effectRenderPath_->SetEnabled("BloomHDR", true);

    RENDERER->SetViewport(0, viewport);
}

Vector3 BabyCam::GetWorldPosition()
{
    return node_->GetWorldPosition();
}

Quaternion BabyCam::GetRotation()
{
    return node_->GetRotation();
}

void BabyCam::Update(float timeStep)
{
//    node_->LookAt(GetScene()->GetChildren().At(Random((int)GetScene()->GetChildren().Size()))->GetWorldPosition());

//    node_->Rotate(Quaternion(100.0f * INPUT->GetMouseMoveY() * timeStep,
//                             0.0f,
//                             100.0f * INPUT->GetMouseMoveX() * timeStep), TS_WORLD);
}

void BabyCam::Lock(SharedPtr<Node> node)
{
}
