
#ifndef WINDOW_H
#define WINDOW_H

#include "sceneobject.h"

class Window : public SceneObject
{
    DRY_OBJECT(Window, SceneObject);
public:
    Window(Context* context);
    static void RegisterObject(Context* context);
    virtual void OnNodeSet(Node* node);
    virtual void Update(float timeStep);

    static HashMap<int, float> timers_;
    void Set(int row, int column);
    void DefenestrateBaby();
private:
    Light* light_;
    int floor_;
    int pId_;
};

#endif // WINDOW_H
