
#ifndef TARGET_H
#define TARGET_H

#include "sceneobject.h"

class Booze : public SceneObject
{
    DRY_OBJECT(Booze, SceneObject);
public:
    Booze(Context* context);
    static void RegisterObject(Context* context);
    virtual void OnNodeSet(Node* node);
    virtual void Update(float timeStep);
    void Set(int pId);
    void HandleNodeCollision(StringHash eventType, VariantMap& eventData);
private:
    int pId_;
};

#endif // TARGET_H
