
#include "baby.h"
#include "player.h"

#include "booze.h"

void Booze::RegisterObject(Context* context)
{
    context->RegisterFactory<Booze>();
}

Booze::Booze(Context* context) : SceneObject(context)
{
}

void Booze::OnNodeSet(Node* node)
{ if(!node) return;
}

void Booze::Set(int pId)
{
    pId_ = pId;

    SceneObject::Set(Vector3(7.0f, 0.0f, pId * LANE_WIDTH));

    StaticModel* tubeModel{ node_->CreateComponent<StaticModel>() };
    tubeModel->SetModel(RES(Model, "Models/Tube.mdl"));
    tubeModel->SetMaterial(MC->GetPlayerMaterial(pId));
    tubeModel->SetCastShadows(true);

    RigidBody* rigidBody{ node_->CreateComponent<RigidBody>() };
    rigidBody->SetRestitution(0.1f);
    rigidBody->SetFriction(10.0f);
    node_->CreateComponent<CollisionShape>()->SetTriangleMesh(RES(Model, "Models/Tube_COLLISION.mdl"));

    SubscribeToEvent(node_, E_NODECOLLISION, DRY_HANDLER(Booze, HandleNodeCollision));
}

void Booze::Update(float timeStep)
{
}

void Booze::HandleNodeCollision(StringHash eventType, VariantMap& eventData)
{ (void)eventType;

    Node* otherNode{ static_cast<Node*>(eventData[NodeCollisionStart::P_OTHERNODE].GetPtr()) };
    Baby* baby{ otherNode->GetComponent<Baby>() };

    if ( baby
     && LucKey::Distance(otherNode->GetWorldPosition(), node_->GetWorldPosition(), true) < 0.2f)
    {
        baby->Disable();
        MC->GetPlayer(pId_)->AddScore(1);

        PlaySample("Tube.ogg", 0.f, 1.f);
    }
}
