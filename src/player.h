
#ifndef PLAYER_H
#define PLAYER_H

#include "mastercontrol.h"

class Controllable;

class Player : public Object
{
    DRY_OBJECT(Player, Object);
public:
    Player(int playerId, Context* context);

    Controllable *GetControllable();

    int GetPlayerId() const { return playerId_; }
    void AddScore(int points);
    unsigned GetScore() const { return score_; }
    void Respawn();
    void ResetScore();

    bool IsHuman() const noexcept { return !autoPilot_; }
    void InitUI();
private:
    Node* scoreNode_;
    Text* scoreText_;
    int playerId_;
    bool autoPilot_;

    unsigned score_;
    int multiplier_;

    void SetScore(int points);
    void CreateUI();
};

#endif // PLAYER_H

