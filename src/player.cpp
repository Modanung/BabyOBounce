
#include "player.h"

#include "mastercontrol.h"
#include "inputmaster.h"

Player::Player(int playerId, Context* context): Object(context),
    playerId_{playerId},
    autoPilot_{false},
    score_{0},
    multiplier_{1}
{
    CreateUI();
}

void Player::CreateUI()
{
    Font* font{ RES(Font, "Fonts/Yikes.ttf") };

    scoreText_ = UI->GetRoot()->CreateChild<Text>("Score");
    scoreText_->SetFont(font, 80);
    scoreText_->SetTextAlignment(HA_CENTER);
    scoreText_->SetTextEffect(TE_SHADOW);
    scoreText_->SetColor(MC->GetPlayerMaterial(playerId_)->GetShaderParameter("MatDiffColor").GetColor());
    scoreText_->SetEffectShadowOffset(IntVector2(2, 10));
    scoreText_->SetEffectColor(Color::BLACK);

    scoreText_->AddTag("Play");

    scoreText_->SetText("0");
    scoreText_->SetVisible(true);
}

void Player::InitUI()
{
    int halfScreenWidth{ GRAPHICS->GetWidth() / 2 };

    scoreText_->SetPosition(IntVector2(halfScreenWidth + (halfScreenWidth / MC->GetNumPlayers()) * playerId_,
                                       0));
}

void Player::Respawn()
{
    ResetScore();
    multiplier_ = 1;
}

void Player::SetScore(int points)
{
    score_ = points;
    scoreText_->SetText(String(score_));
}
void Player::ResetScore()
{
    SetScore(0);
}

void Player::AddScore(int points)
{
    SetScore(score_ + points);
}

Controllable* Player::GetControllable()
{
    return GetSubsystem<InputMaster>()->GetControllableByPlayer(playerId_);
}

