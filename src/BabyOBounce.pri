HEADERS += \
    $${PWD}/luckey.h \
    $${PWD}/mastercontrol.h \
    $${PWD}/sceneobject.h \
    $${PWD}/spawnmaster.h \
    $${PWD}/inputmaster.h \
    $${PWD}/player.h \
    $${PWD}/controllable.h \
    $${PWD}/effectmaster.h \
    $${PWD}/window.h \
    $${PWD}/baby.h \
    $${PWD}/babycam.h \
    $${PWD}/trampoline.h \
    $${PWD}/tree.h \
    $${PWD}/booze.h

SOURCES += \
    $${PWD}/luckey.cpp \
    $${PWD}/mastercontrol.cpp \
    $${PWD}/sceneobject.cpp \
    $${PWD}/spawnmaster.cpp \
    $${PWD}/inputmaster.cpp \
    $${PWD}/player.cpp \
    $${PWD}/controllable.cpp \
    $${PWD}/effectmaster.cpp \
    $${PWD}/window.cpp \
    $${PWD}/baby.cpp \
    $${PWD}/babycam.cpp \
    $${PWD}/trampoline.cpp \
    $${PWD}/tree.cpp \
    $${PWD}/booze.cpp
