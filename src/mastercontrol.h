
#ifndef MASTERCONTROL_H
#define MASTERCONTROL_H

#include "luckey.h"

#define LANE_WIDTH 2.0f
#define MAX_PLAYERS 6

namespace Dry {
class Node;
class Scene;
}

class Player;
class Window;

class MasterControl : public Application
{
    DRY_OBJECT(MasterControl, Application);
public:
    MasterControl(Context* context);
    static MasterControl* GetInstance();

    Scene* GetScene() const { return scene_; }
    bool IsGoreEnabled() const { return gore_; }
    void AddPlayer();
    int GetNumPlayers() const { return numPlayers_; }
    Player* GetPlayer(int playerId) const;
    Player* GetNearestPlayer(Vector3 pos);
    Vector< SharedPtr<Player> > GetPlayers();
    void RemovePlayer(Player* player);
    Material* GetPlayerMaterial(int pId) const { return playerMaterials_[pId]->Get(); }

    virtual void Setup();
    virtual void Start();
    virtual void Stop();
    void Exit();
    void HandleSceneUpdate(StringHash eventType, VariantMap& eventData);


private:
    void CreateScene();
    void CollectPlayerMaterials();
    void ProcessArguments();

    static MasterControl* instance_;

    Scene* scene_;
    int numPlayers_;
    bool gore_;
    Vector< SharedPtr<Player> > players_;
    HashMap<int, Vector<Window*>> stacks_;
    HashMap<int, float> counters_;
    HashMap<int, SharedPtr<Material>> playerMaterials_;

};

#endif // MASTERCONTROL_H

