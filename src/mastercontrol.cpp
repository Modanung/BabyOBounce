
#include "effectmaster.h"
#include "inputmaster.h"
#include "player.h"

#include "spawnmaster.h"
#include "babycam.h"
#include "window.h"
#include "baby.h"
#include "trampoline.h"
#include "booze.h"
#include "tree.h"

#include "mastercontrol.h"

DRY_DEFINE_APPLICATION_MAIN(MasterControl);

MasterControl* MasterControl::instance_ = NULL;

MasterControl* MasterControl::GetInstance()
{
    return MasterControl::instance_;
}

MasterControl::MasterControl(Context* context): Application(context),
    scene_{ nullptr },
    numPlayers_{ 3 },
    gore_{ true },
    players_{},
    stacks_{},
    counters_{},
    playerMaterials_{}
{
    ProcessArguments();

    instance_ = this;
    numPlayers_ = Min(MAX_PLAYERS, numPlayers_);
}

void MasterControl::Setup()
{
    SetRandomSeed(TIME->GetSystemTime());

    engineParameters_[EP_LOG_NAME] = GetSubsystem<FileSystem>()->GetAppPreferencesDir("luckey", "logs")+"BabyOBounce.log";
    engineParameters_[EP_WINDOW_TITLE] = "Baby-O-Bounce";
    engineParameters_[EP_WINDOW_ICON] = "Textures/Baby.png";
    engineParameters_[EP_WORKER_THREADS] = false;
    engineParameters_[EP_RESOURCE_PATHS] = "Resources";
}

void MasterControl::Start()
{
    engine_->SetMaxFps(50);

    context_->RegisterSubsystem(new EffectMaster(context_));
    context_->RegisterSubsystem(new InputMaster(context_));
    context_->RegisterSubsystem(new SpawnMaster(context_));

    BabyCam::RegisterObject(context_);
    Window::RegisterObject(context_);
    Baby::RegisterObject(context_);
    Trampoline::RegisterObject(context_);
    Booze::RegisterObject(context_);
    Tree::RegisterObject(context_);

    CollectPlayerMaterials();

    CreateScene();

    SoundSource* musicSource{ scene_->CreateComponent<SoundSource>() };
    musicSource->SetSoundType(SOUND_MUSIC);
    SharedPtr<Sound> music{ RES(Sound, "Music/Loyalty Freak Music - MEGA METAL.ogg") };
    music->SetLooped(true);
    musicSource->Play(music);
    musicSource->SetGain(.17f);

    SubscribeToEvent(E_SCENEUPDATE, DRY_HANDLER(MasterControl, HandleSceneUpdate));
}

void MasterControl::ProcessArguments()
{
    for (const String& arg: GetArguments())
    {
        const StringVector equalsSplit{ arg.Split('=', false) };
        const bool equate{ equalsSplit.Size() == 2u };

        if (equate || arg.StartsWith("-"))
        {
            String parameter{ (equate ? equalsSplit.Front() : arg) };
            while (parameter.Front() == '-')
                parameter = parameter.Substring(1u);
            if (parameter.IsEmpty())
                continue;

            const int  valueInt{  (equate ? ToInt( equalsSplit.Back()) : 1) };
            const bool valueBool{ (equate ? ToBool(equalsSplit.Back()) : false) };

            if (parameter.StartsWith("P", false) && valueInt != 0)
                numPlayers_ = valueInt;
            else if (parameter.StartsWith("G", false))
                gore_ = valueBool;
        }
    }
}

void MasterControl::Stop()
{
    engine_->DumpResources(true);
}
void MasterControl::Exit()
{
    engine_->Exit();
}

void MasterControl::CollectPlayerMaterials()
{
    for (int p{ 0 }; p < MAX_PLAYERS; ++p)
    {
        String color{};

        switch (p) {
        case 0: color = "Red";      break;
        case 1: color = "Green";    break;
        case 2: color = "Blue";     break;
        case 3: color = "Magenta";  break;
        case 4: color = "Yellow";   break;
        case 5: color = "Cyan";     break;
        default:                    break;
        }

        playerMaterials_[p] = RES(Material, "Materials/" + color + ".xml");
    }
}

void MasterControl::CreateScene()
{
    scene_ = new Scene(context_);
    scene_->CreateComponent<Octree>();
    scene_->CreateComponent<PhysicsWorld>()->SetGravity(Vector3::DOWN * 5.0f);

    scene_->CreateChild("Camera")->CreateComponent<BabyCam>();

    //Create floor
    Node* floorNode{ scene_->CreateChild("Floor") };
    floorNode->AddTag("Fatal");
    Node* floorModelNode{ floorNode->CreateChild("FloorModel") };
    StaticModel* floorModel{ floorModelNode->CreateComponent<StaticModel>() };
    floorModel->SetModel(RES(Model, "Models/Ground.mdl"));
    floorModel->SetMaterial(RES(Material, "Materials/VCol.xml"));

    RigidBody* floorBody{ floorNode->CreateComponent<RigidBody>() };
    floorBody->SetRestitution(0.125f);
    floorBody->SetCollisionLayer(4);
    floorNode->CreateComponent<CollisionShape>()->SetStaticPlane(Vector3::ZERO);

    Node* moonNode{ scene_->CreateChild("Moon") };
    moonNode->SetPosition(Vector3(5.0f, 25.0f, 0.0f));
    moonNode->LookAt(Vector3::ZERO);
    Light* moonLight{ moonNode->CreateComponent<Light>() };
    moonLight->SetLightType(LIGHT_DIRECTIONAL);
    moonLight->SetBrightness(0.42f);
    moonLight->SetCastShadows(true);
    moonLight->SetShadowIntensity(0.7f);

    int numFloors{ 5 };

    Node* spotNode{ scene_->CreateChild("SpotLight") };
    spotNode->SetPosition(Vector3(9.0f, 2.0f, -3.0f));
    spotNode->LookAt(Vector3(-5.0f, numFloors * 0.75f, numPlayers_ * 0.5f));

    Light* spotLight{ spotNode->CreateComponent<Light>() };
    spotLight->SetLightType(LIGHT_SPOT);
    spotLight->SetBrightness(1.8f);
    spotLight->SetRange(25.0f);
    spotLight->SetFov(35.0f);
    spotLight->SetCastShadows(true);
    spotLight->SetShadowIntensity(0.8f);

    Node* backNode{ scene_->CreateChild("BackWall") };
    backNode->SetPosition(Vector3::RIGHT * 8.0f);
    backNode->CreateComponent<RigidBody>();
    backNode->CreateComponent<CollisionShape>()->SetBox(Vector3(1.0f, 50.0f, 50.0f));

    for (int p{ 0 }; p < numPlayers_; ++p) {

        Player* player{ new Player(p, context_) };
        players_.Push(SharedPtr<Player>(player));
        counters_[p] = Random(2.0f, 3.0f);

        Trampoline* trampoline{ SPAWN->Create<Trampoline>() };
        trampoline->Set(p);
        GetSubsystem<InputMaster>()->SetPlayerControl(player, trampoline);

        Booze* tube{ SPAWN->Create<Booze>() };
        tube->Set(p);

        for (int i{ 0 }; i < numFloors; ++i) {

            Window* window{ SPAWN->Create<Window>() };
            stacks_[p].Push(window);
            window->Set(i, p);
        }
    }

    for (Player* p : GetPlayers())
    {
        p->InitUI();
    }

    for (int t{ 0 }; t < 4; ++t) {

        SPAWN->Create<Tree>()->Set(Vector3(8.0f, 0.0f, -2.0f + 4.0f * t));
    }
}

void MasterControl::HandleSceneUpdate(StringHash eventType, VariantMap &eventData)
{ (void)eventType;

    for (Player* p : players_)
    {
        int pId{ p->GetPlayerId() };
        counters_[pId] -= eventData[SceneUpdate::P_TIMESTEP].GetFloat();

        if (counters_[pId] < 0.0f)
        {
            counters_[pId] = 0.1f + 0.8f * Random(1, 5);

            int floors{ static_cast<int>(stacks_[pId].Size()) };
            stacks_[pId].At(Random(floors))->DefenestrateBaby();
        }
    }
}

Vector<SharedPtr<Player> > MasterControl::GetPlayers()
{
    return players_;
}
Player* MasterControl::GetPlayer(int playerId) const
{
    for (Player* p : players_) {

        if (p->GetPlayerId() == playerId){
            return p;
        }
    }
    return nullptr;
}
Player* MasterControl::GetNearestPlayer(Vector3 pos)
{
    Player* nearest{};
    for (Player* p : players_){
        if (!nearest
                || (LucKey::Distance(GetSubsystem<InputMaster>()->GetControllableByPlayer(p->GetPlayerId())->GetWorldPosition(), pos) <
                    LucKey::Distance(GetSubsystem<InputMaster>()->GetControllableByPlayer(nearest->GetPlayerId())->GetWorldPosition(), pos)))
        {
            nearest = p;
        }
    }
    return nearest;
}

