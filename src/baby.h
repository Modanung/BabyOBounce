
#ifndef BABY_H
#define BABY_H

#include "sceneobject.h"

class Baby : public SceneObject
{
    DRY_OBJECT(Baby, SceneObject);
public:
    Baby(Context* context);
    static void RegisterObject(Context* context);
    virtual void OnNodeSet(Node* node);
    virtual void Update(float timeStep);
    void Set(Vector3 position, int pId);
    virtual void Disable();
    void HandleNodeCollisionStart(StringHash eventType, VariantMap& eventData);
    void HandleNodeCollision(StringHash eventType, VariantMap& eventData);
    void HandleNodeCollisionEnd(StringHash eventType, VariantMap& eventData);
private:
    void Die();

    static HashMap<int, StaticModelGroup*> babies_;
    RigidBody* rigidBody_;
    Node* bloodNode_;
    ParticleEmitter* bloodEmitter_;
    int pId_;
    bool alive_;
};

#endif // BABY_H
