
#include "window.h"

#include "spawnmaster.h"
#include "baby.h"

void Window::RegisterObject(Context* context)
{
    context->RegisterFactory<Window>();
}

Window::Window(Context* context): SceneObject(context),
    light_{ nullptr },
    floor_{},
    pId_{}
{
}

void Window::OnNodeSet(Node* node)
{ if(!node) return;

    AnimatedModel* model{ node_->CreateComponent<AnimatedModel>() };
    model->SetModel(RES(Model, "Models/Window.mdl"));
    model->SetMaterial(0, RES(Material, "Materials/Wall.xml"));
    model->SetMaterial(1, RES(Material, "Materials/Carpet.xml"));
    model->SetCastShadows(true);

    Node* particleNode{ node_->CreateChild("Fire") };
    particleNode->CreateComponent<ParticleEmitter>()->SetEffect(RES(ParticleEffect, "Particles/Fire.xml"));
    particleNode->SetPosition(Vector3(0.1f, 0.2f, 0.0f));
    light_ = node_->CreateChild("Light")->CreateComponent<Light>();
    light_->SetRange(5.0f);
    light_->SetCastShadows(true);
    light_->SetShadowResolution(0.125f);
}

void Window::Set(int floor, int pId)
{
    SceneObject::Set(Vector3(-7.0f, floor + 0.5f, pId * LANE_WIDTH));
    if (floor == 0)
        node_->GetComponent<AnimatedModel>()->SetMorphWeight(0, 0.5f);

    floor_ = floor;
    pId_ = pId;

    node_->GetComponent<AnimatedModel>()->SetMaterial(2, MC->GetPlayerMaterial(pId));

}

void Window::Update(float timeStep)
{ (void)timeStep;
//    light_->SetColor(Color(LucKey::Sine(TIME->GetElapsedTime(),)));
    light_->SetBrightness(0.5f - 0.25f * (LucKey::Sine(TIME->GetElapsedTime() * (10.0f + randomizer_) + 500.0f * randomizer_)));
    light_->GetNode()->SetPosition(Vector3(-0.25f,
                                           0.5f + 0.125f * LucKey::Sine(TIME->GetElapsedTime() * 2.0f + randomizer_ * 25.0f),
                                           0.0125f * LucKey::Sine(TIME->GetElapsedTime() * 1.5f + randomizer_ * 5.0f)));
}

void Window::DefenestrateBaby()
{
    SPAWN->Create<Baby>()->Set(node_->GetPosition() + Vector3(-0.5f, 0.0f, -0.125f), pId_);

    PlaySample("Woosh.ogg", 60000.f + Random() * 30000.f, .42f);
}



