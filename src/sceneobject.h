
#ifndef SCENEOBJECT_H
#define SCENEOBJECT_H

#include "mastercontrol.h"

class SceneObject : public LogicComponent
{
    DRY_OBJECT(SceneObject, Object);
public:
    SceneObject(Context* context);

    virtual void Set(Vector3 position);
    virtual void OnNodeSet(Node *node);
    virtual void Disable();

    Vector3 GetWorldPosition() const;

protected:
    float randomizer_;

    void PlaySample(String sampleName, float frequency = 0.f, float gain = 0.3f);
};

#endif // SCENEOBJECT_H

