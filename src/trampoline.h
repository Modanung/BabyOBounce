
#ifndef TRAMPOLINE_H
#define TRAMPOLINE_H

#include "controllable.h"

#define TRAMPOLINE_MAXPOSX 5.6f

class Trampoline: public Controllable
{
    DRY_OBJECT(Trampoline, Controllable);
public:
    Trampoline(Context* context);
    static void RegisterObject(Context* context);
    virtual void OnNodeSet(Node* node);
    virtual void Update(float timeStep);
    void HandleNodeCollisionStart(StringHash eventType, VariantMap& eventData);
    virtual void Set(int pId);

private:
    void Move(float timeStep);
    void Stretch(float timeStep);

    AnimatedModel* trampolineModel_;
    int pId_;
    float sinceHit_;
    float maxMoveSpeed_;
    float moveSpeed_;
};

#endif // TRAMPOLINE_H
