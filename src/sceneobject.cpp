
#include "sceneobject.h"

SceneObject::SceneObject(Context *context): LogicComponent(context),
    randomizer_{ Random() }
{
}

void SceneObject::OnNodeSet(Node *node)
{ if (!node) return;
}

void SceneObject::Set(Vector3 position)
{
    node_->SetPosition(position);
    node_->SetEnabledRecursive(true);
}

void SceneObject::Disable()
{
    node_->SetEnabledRecursive(false);
}

void SceneObject::PlaySample(String sampleName, float frequency, float gain)
{
    SoundSource3D* source{ node_->CreateComponent<SoundSource3D>() };
    source->SetAngleAttenuation(666.f, 666.f);
    source->SetAutoRemoveMode(REMOVE_COMPONENT);
    source->Play(RES(Sound, String{ "Samples/" } + sampleName), frequency, gain);
}

Vector3 SceneObject::GetWorldPosition() const
{
    return node_->GetWorldPosition();
}

