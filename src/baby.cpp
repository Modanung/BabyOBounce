#include "baby.h"

HashMap<int, StaticModelGroup*> Baby::babies_;

void Baby::RegisterObject(Context* context)
{
    context->RegisterFactory<Baby>();
}

Baby::Baby(Context* context) : SceneObject(context),
    alive_{ true }
{
}

void Baby::OnNodeSet(Node* node)
{ if(!node) return;

    bloodNode_ = GetScene()->CreateChild("Blood");
    bloodEmitter_ = bloodNode_->CreateComponent<ParticleEmitter>();
    if (MC->IsGoreEnabled())
        bloodEmitter_->SetEffect(RES(ParticleEffect, "Particles/Blood.xml"));
    bloodEmitter_->SetEmitting(false);

    if (!babies_.Size()) {
        for (int p{ 0 }; p < MAX_PLAYERS; ++p) {

            babies_[p] = GetScene()->CreateComponent<StaticModelGroup>();
            babies_[p]->SetModel(RES(Model, "Models/Baby.mdl"));
            babies_[p]->SetCastShadows(true);
            babies_[p]->SetMaterial(0, RES(Material, "Materials/Diaper.xml"));
            babies_[p]->SetMaterial(1, RES(Material, "Materials/Skin.xml"));
            babies_[p]->SetMaterial(2, MC->GetPlayerMaterial(p));
        }
    }

    rigidBody_ = node_->CreateComponent<RigidBody>();
    rigidBody_->SetMass(10.0f);
    rigidBody_->SetRestitution(1.0f);
    rigidBody_->SetFriction(0.0f);
    rigidBody_->SetLinearFactor(Vector3(1.0f, 1.0f, 0.0f));
    rigidBody_->SetCollisionLayer(2);
    rigidBody_->SetCollisionMask(M_MAX_UNSIGNED - 6);
    node_->CreateComponent<CollisionShape>()->SetSphere(0.4f);

    SubscribeToEvent(node_, E_NODECOLLISIONSTART, DRY_HANDLER(Baby, HandleNodeCollisionStart));
    SubscribeToEvent(node_, E_NODECOLLISION, DRY_HANDLER(Baby, HandleNodeCollision));
    SubscribeToEvent(node_, E_NODECOLLISIONEND, DRY_HANDLER(Baby, HandleNodeCollisionEnd));
}

void Baby::Set(Vector3 position, int pId)
{
    alive_ = true;

    node_->SetWorldScale(Vector3::ONE);
    node_->AddTag("Bounce");

    rigidBody_->SetRestitution(1.0f);
    rigidBody_->SetFriction(0.0f);
    rigidBody_->SetLinearFactor(Vector3(1.0f, 1.0f, 0.0f));
    rigidBody_->SetCollisionLayer(2);
    rigidBody_->SetCollisionMask(M_MAX_UNSIGNED - 2);
    node_->GetComponent<CollisionShape>()->SetSphere(0.4f);

    pId_ = pId;

    babies_[pId]->AddInstanceNode(node_);

    rigidBody_->SetLinearVelocity(Vector3::ZERO);
    rigidBody_->SetAngularVelocity(Vector3::ZERO);
    rigidBody_->ResetForces();
    rigidBody_->ReAddBodyToWorld();

    SceneObject::Set(position);
    rigidBody_->ApplyImpulse(Vector3(Random(20.0f, 25.0f), 50.0f - node_->GetPosition().y_ * 5.0f, 0.0f));
    rigidBody_->ApplyTorqueImpulse(Vector3(Random(-1.0f, 1.0f),
                                           Random(-1.0f, 1.0f),
                                           Random(-1.0f, 1.0f)));


    bloodEmitter_->SetEmitting(false);
}

void Baby::Disable()
{
    SceneObject::Disable();
    babies_[pId_]->RemoveInstanceNode(node_);
}

void Baby::Update(float timeStep)
{
    if (node_->GetPosition().x_ > 10.0f)
        Disable();
}

void Baby::HandleNodeCollisionStart(StringHash eventType, VariantMap& eventData)
{
    Node* otherNode{ static_cast<Node*>(eventData[NodeCollisionStart::P_OTHERNODE].GetPtr()) };

    if (otherNode->HasTag("Fatal") || !alive_)
    {
        bloodEmitter_->SetEmitting(true);
        if (alive_)
        {
            Die();

            PlaySample("Splat.ogg", 30000.f + Random() * 10000.f, .55f);
        }
        else
        {
            PlaySample("Squish.ogg", 60000.f + Random() * 30000.f, .11f);
        }
    }
}
void Baby::HandleNodeCollision(StringHash eventType, VariantMap& eventData)
{ (void)eventType;

    if (alive_)
        return;

    Node* otherNode{ static_cast<Node*>(eventData[NodeCollisionStart::P_OTHERNODE].GetPtr()) };
    MemoryBuffer contacts{ eventData[NodeCollisionStart::P_CONTACTS].GetBuffer() };

//    if (otherNode->HasTag("Fatal")) {

        node_->SetWorldScale(node_->GetWorldScale() *
                             Vector3(1.0f,
                                     1.0f - 5.0f * TIME->GetTimeStep() * Max(1.0f, rigidBody_->GetLinearVelocity().Length()),
                                     1.0f));

        Vector3 scale{ node_->GetWorldScale() };
        float volume{ scale.x_ * scale.y_ * scale.z_ };

        if (volume < 0.1f)
            Disable();

        while (!contacts.IsEof())
        {
            Vector3 contactPosition{ contacts.ReadVector3() };
            //        Vector3 contactNormal{ contacts.ReadVector3() };
            //        float contactDistance{ contacts.ReadFloat() };
            //        float contactImpulse{ contacts.ReadFloat() };

            bloodNode_->SetPosition(node_->GetWorldPosition());
        }
//    }
}
void Baby::HandleNodeCollisionEnd(StringHash eventType, VariantMap& eventData)
{ (void)eventType;

    bloodEmitter_->SetEmitting(false);
}

void Baby::Die()
{
    alive_ = false;

    node_->RemoveTag("Bounce");

    rigidBody_->SetCollisionLayer(3);
    rigidBody_->SetCollisionMask(M_MAX_UNSIGNED - 2);

    rigidBody_->SetRestitution(0.5f);
    rigidBody_->SetFriction(0.2f);
    rigidBody_->SetLinearFactor(Vector3::ONE);
    node_->GetComponent<CollisionShape>()->SetBox(Vector3::ONE * 0.2f);
}



