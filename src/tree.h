
#ifndef TREE_H
#define TREE_H

#include "sceneobject.h"

class Tree : public SceneObject
{
    DRY_OBJECT(Tree, SceneObject);
public:
    Tree(Context* context);
    static void RegisterObject(Context* context);
    virtual void OnNodeSet(Node* node);
    virtual void Update(float timeStep);
    void HandleNodeCollision(StringHash eventType, VariantMap& eventData);
};

#endif // TREE_H
