# Baby-O-Bounce
A remake of a QBasic game called Baby.

### Commandline arguments

| Value        | Argument |  Type   |
|:------------:|:--------:|:-------:|
| Player count | -P=...   | Number  |
| Gore         | -G=...   | Boolean |

### Controls

| Player | Left | Right |
|:------:|:---:|:---:|
| 1 | **Q** | **W** |
| 2 | **C** | **V** |
| 3 | **J** | **K** |
| 4 | **[** | **]** |
| 5 | **Numpad 2** | **Numpad 3** |
| 6 | **Numpad +** | **Numpad -** |

![Screenshot](Screenshots/Screenshot_Thu_Sep__6_00_41_44_2018.png)
